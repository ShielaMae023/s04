from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class ToDoItem(models.Model) :
	task_name = models.CharField(max_length=50)
	description= models.CharField(max_length=200)
	status=models.CharField(max_length=50, default="Pending")
	date_created=models.DateTimeField('date created')
	user = models.ForeignKey(User, on_delete=models.CASCADE, default="")


class register(models.Model):
	username = models.CharField(max_length=50)
	firstname = models.CharField(max_length=50)
	lastname = models.CharField(max_length=50)
	email = models.CharField(max_length=50)
	password = models.CharField(max_length=50)
	confirm_password = models.CharField(max_length=50)

class event(models.Model):
	event_name=models.CharField(max_length=50)
	description=models.CharField(max_length=200)