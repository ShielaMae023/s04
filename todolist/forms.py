from django import forms


class LoginForm(forms.Form):
	username = forms.CharField(label='Username', max_length=20)
	password= forms.CharField(label='Password', max_length=20)

class AddTaskForm(forms.Form):
	task_name = forms.CharField(label='Task Name', max_length=50)
	description= forms.CharField(label='Description', max_length=200)

class UpdateTaskForm(forms.Form):
	task_name = forms.CharField(label='Task Name', max_length=50)
	description= forms.CharField(label='Description', max_length=200)
	status = forms.CharField(label= 'Status', max_length=50)

class register(forms.Form):
	username = forms.CharField(label='Username:', max_length=50)
	firstname = forms.CharField(label='First Name:', max_length=50)
	lastname = forms.CharField(label='Last Name:', max_length=50)
	email = forms.CharField(label='Email:', max_length=50)
	password = forms.CharField(label='Password:', max_length=50)
	confirm_password = forms.CharField(label='Confirm Password:', max_length=50)
	

	def save(self, commit=True):
		user=super(register, self),save(commit=False)
		user.email=self.cleaned_data['email']
		if commit:
			user.save()
		return user


class add_event(forms.Form):
	eventname = forms.CharField(label='Event Name', max_length=50)
	description= forms.CharField(label='Description', max_length=200)
	status=forms.CharField(label='Status', max_length=50)
